# Timber

Tiny logger frontend:

- `IBackend` interface
- `Logger` object
- Logging macros

[Example](example/main.cpp)
