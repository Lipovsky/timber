#pragma once

#include <timber/log/level.hpp>
#include <timber/log/level/string.hpp>

#include <fmt/format.h>

template <>
struct fmt::formatter<timber::log::Level>: formatter<string_view> {
  // parse is inherited from formatter<string_view>.
  template <typename FormatContext>
  auto format(timber::log::Level level, FormatContext& ctx) {
    return formatter<string_view>::format(timber::log::LevelToString(level), ctx);
  }
};
