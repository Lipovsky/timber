#pragma once

#include <timber/log/level.hpp>

#include <string>

namespace timber::log {

// Level::Warning <-> "Warning"

std::string_view LevelToString(Level level);
Level LevelFromString(const std::string& level_str);

}  // namespace timber::log
