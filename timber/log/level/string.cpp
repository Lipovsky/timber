#include <timber/log/level/string.hpp>

#include <stdexcept>
#include <sstream>

namespace timber::log {

std::string_view LevelToString(Level level) {
#define LEVEL_TO_STRING_CASE(level) \
case Level::level: return #level

  switch (level) {
    LEVEL_TO_STRING_CASE(Trace);
    LEVEL_TO_STRING_CASE(Debug);
    LEVEL_TO_STRING_CASE(Info);
    LEVEL_TO_STRING_CASE(Warning);
    LEVEL_TO_STRING_CASE(Error);
    LEVEL_TO_STRING_CASE(Critical);
    default: return "?";
  }

#undef LEVEL_TO_STRING_CASE
}

Level LevelFromString(const std::string& level_str) {
#define FROM_STRING_IF(level) \
if (level_str == #level) {  \
return Level::level;      \
}

  FROM_STRING_IF(Trace);
  FROM_STRING_IF(Debug);
  FROM_STRING_IF(Info);
  FROM_STRING_IF(Warning);
  FROM_STRING_IF(Error);
  FROM_STRING_IF(Critical);
  FROM_STRING_IF(All);
  FROM_STRING_IF(Off);

#undef FROM_STRING_IF

  std::stringstream error_out;
  error_out << "Unknown logging level name: '" << level_str << "'";

  throw std::runtime_error(error_out.str());
}

}  // namespace timber::log
