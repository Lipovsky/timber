#pragma once

#include <timber/log/backend.hpp>
#include <timber/log/ostream/level.hpp>

namespace timber::log::backends {

class NopBackend : public IBackend {
 public:
  Level GetMinLevelFor(
      const std::string& /*component*/) const override {
    return Level::Off;
  }

  void Log(Record /*ignored*/) override {
    // No-op
  }
};

}  // namespace timber::log::backends
