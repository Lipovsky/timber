#pragma once

#include <timber/log/extra.hpp>

#include <fmt/format.h>
#include <fmt/ostream.h>
#include <fmt/chrono.h>

#include <ostream>

namespace timber::log {

struct WithExtra {
  const ExtraFields& headers;
  const std::string& message;
  const ExtraFields& trailers;
};

std::ostream& operator<<(std::ostream& out, WithExtra with_extra);

}  // namespace timber::log

namespace fmt {

template<>
struct formatter<timber::log::WithExtra> : ostream_formatter {};

}  // namespace fmt
