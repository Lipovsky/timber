#pragma once

#include <string>
#include <vector>

namespace timber::log {

struct ExtraField {
  std::string key;
  std::string value;
};

using ExtraFields = std::vector<ExtraField>;

}  // namespace timber::log
