#pragma once

#include <timber/log/level.hpp>
#include <timber/log/extra.hpp>

#include <string>

namespace timber::log {

struct Record {
  struct Extra {
    ExtraFields logger;
    ExtraFields site;
  };

  Level level;
  std::string component;
  std::string message;
  Extra extra;
};

}  // namespace timber::log
