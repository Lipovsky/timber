#include <timber/log/logger.hpp>
#include <timber/log/log.hpp>

#include <timber/log/backends/stdout/backend.hpp>

//////////////////////////////////////////////////////////////////////

timber::log::backends::StdoutBackend backend;

int main() {
  timber::log::Logger logger_(/*component=*/"main", backend);

  // Add extra key/value fields to all records for this logger
  logger_.AddExtra({"key", "value"});

  // Implicitly use `logger_`
  TIMBER_LOG_DEBUG("Hello, world!");
  TIMBER_LOG_INFO("{} + {} = {}", 2, 2, 4);

  {
    // Explicitly set logging level
    auto level = (2 + 2 == 4) ? timber::log::Level::Debug : timber::log::Level::Error;
    TIMBER_LOG(level, "Set level");
  }
}
